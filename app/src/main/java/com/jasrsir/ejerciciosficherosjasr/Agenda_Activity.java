package com.jasrsir.ejerciciosficherosjasr;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.jasrsir.ejerciciosficherosjasr.classes.AdapterAmigoos;
import com.jasrsir.ejerciciosficherosjasr.classes.AddAmigo;
import com.jasrsir.ejerciciosficherosjasr.classes.Midiario;
import com.jasrsir.ejerciciosficherosjasr.classes.SerialAmigos;

public class Agenda_Activity extends ListActivity {

    private static final String ARCHIVO = "agendaAmigos.txt";
    private AdapterAmigoos adapterAmigoos;
    private SerialAmigos serialAmigos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);
        //Inicializamos fichero, adapter
        serialAmigos = new SerialAmigos(getApplicationContext().getFilesDir().getAbsolutePath(), ARCHIVO);
        serialAmigos.sacarDiario();
        adapterAmigoos = new AdapterAmigoos(Agenda_Activity.this, Midiario.getMidiario());
        getListView().setAdapter(adapterAmigoos);
    }

    public void onClickAddAmigo(View view) {
        startActivity(new Intent(Agenda_Activity.this, AddAmigo.class));
        adapterAmigoos.notifyDataSetChanged();
    }

    public void onClickDeleteAmigo(View view) {
        adapterAmigoos.remove(view.getId());

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Informa a la lista de que puede que halla cambios
        adapterAmigoos.notifyDataSetChanged();

    }
}
