package com.jasrsir.ejerciciosficherosjasr;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class Alarma_Activity extends AppCompatActivity {

    EditText mIntervalo;
    EditText mMensaje;
    Button start;
    CountDownTimer contador;
    TextView cronometro;
    TextView alarmas;
    int mTime;
    long mLongTime;
    int nAlarma = 0;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarma);
        mIntervalo = (EditText) findViewById(R.id.edt_intervalo);
        mMensaje = (EditText) findViewById(R.id.edt_mensaje);
        start = (Button) findViewById(R.id.btn_empezar);
        cronometro = (TextView) findViewById(R.id.chronometer3);
        alarmas = (TextView) findViewById(R.id.textView5);
        file = new File(Environment.getExternalStorageDirectory() + "/" + "alarmitas.txt");


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(mIntervalo.getText()) && !TextUtils.isEmpty(mMensaje.getText())) {
                    start.setEnabled(false);
                    mTime = Integer.parseInt(mIntervalo.getText().toString());
                    mLongTime = mTime * 60 * 1000;
                    guardar();

                    sonar(mLongTime);
                } else
                    Toast.makeText(getApplicationContext(), "No puedes tener campos vacíos", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void guardar() {
        BufferedWriter bufferEscritor;

        try {
            if (!file.exists())
                file.createNewFile();
            bufferEscritor = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory() + "/" + file.getName()));
            Toast.makeText(this, "Alarma guardada en  " +file.getAbsolutePath(), Toast.LENGTH_LONG).show();

            for (int i = 1; i < 6; i++) {
                bufferEscritor.append("Nº Alarma: " + i + ". Durará: " + mIntervalo.getText().toString() + ". Y contiene este mensaje: " + mMensaje.getText().toString());
                bufferEscritor.newLine();
                bufferEscritor.flush();
            }
            if (bufferEscritor != null)
                bufferEscritor.close();
        } catch (Exception e) {
            Log.e("ERROR WRITING", "Unable to write" + e.getMessage());
        }
    }

    private void sonar(long t) {

        cronometro.setText(DateFormat.format("mm:ss", t));
        contador = new CountDownTimer(t, 1000) {
            @Override
            public void onTick(long l) {
                cronometro.setText(DateFormat.format("mm:ss", l));
            }

            @Override
            public void onFinish() {
                cronometro.setText("00:00");
                Toast.makeText(getApplicationContext(), "Fin alarma " + ++nAlarma + "   Mensaje: " + mMensaje.getText().toString(), Toast.LENGTH_LONG).show();
                alarmas.setText("Alarmas finalizadas: " + nAlarma + "/5");
                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.camion);
                mp.start();
                if (nAlarma < 5) {
                    cronometro.setText(DateFormat.format("mm:ss", mLongTime));
                    contador.start();
                } else
                    start.setEnabled(true);
            }
        };
        contador.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "Alarma cancelada", Toast.LENGTH_LONG).show();
        if (contador != null)
            contador.cancel();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (contador != null)
            contador.start();
    }
}