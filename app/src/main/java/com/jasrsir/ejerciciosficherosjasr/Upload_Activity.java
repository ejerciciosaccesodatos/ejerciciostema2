package com.jasrsir.ejerciciosficherosjasr;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jasrsir.ejerciciosficherosjasr.classes.RestClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import cz.msebera.android.httpclient.Header;

public class Upload_Activity extends AppCompatActivity {

    private Button benBuscar, btnSubir;
    private TextView nombreArchivoElegido;
    private File archivo;
    public final static String WEB = "https://ejerciciostema2.000webhostapp.com/upload.php";

    private static final int ABRIRFICHERO_REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        benBuscar = (Button) findViewById(R.id.buscar);
        btnSubir = (Button) findViewById(R.id.subir);
        nombreArchivoElegido = (TextView) findViewById(R.id.nombredelarchivo);
        benBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                //Si hay una aplicación que realice lo que quiero...
                if (intent.resolveActivity(getPackageManager()) != null)
                    startActivityForResult(intent, ABRIRFICHERO_REQUEST_CODE);
                else //informar que no hay ninguna aplicación para manejar ficheros
                    Toast.makeText(Upload_Activity.this, "No hay aplicación para manejar ficheros", Toast.LENGTH_SHORT).show();
            }


        });
        btnSubir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Comprobamos que tenemos conexion
                if (hayInternet()) {
                    if (archivo != null)
                        subida(v);
                    else
                        Toast.makeText(Upload_Activity.this, "No hay un archivo seleccionado", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(Upload_Activity.this, "No hay una conexion a la red", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ABRIRFICHERO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                //Mostramos en la etiqueta el resultado
                archivo = new File(data.getData().getPath());
                nombreArchivoElegido.setText(data.getData().getPath());
            } else //informar que no ha cogido ningun archivo
                Toast.makeText(this, "No has seleccionado nada.", Toast.LENGTH_SHORT).show();
        }

    }


    public void subida(View v) {
        String fichero = nombreArchivoElegido.getText().toString();
        final ProgressDialog progreso = new ProgressDialog(this);
        File myFile;
        Boolean existe = true;
        myFile = new File(archivo.getAbsolutePath()); //File myFile = new File("/path/to/file.png");
        RequestParams params = new RequestParams();
        try {
            params.put("fileToUpload", myFile);
        } catch (FileNotFoundException e) {
            existe = false;
            Toast.makeText(this, "Error en el fichero: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        if (existe) {
            RestClient.post(WEB, params, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    // called before request is started
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Conectando . . ."); //progreso.setCancelable(false);
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            RestClient.cancelRequests(getApplicationContext(), true);
                        }
                    });
                    progreso.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String response) { // called when response HTTP status is "200 OK"
                    progreso.dismiss();
                    Toast.makeText(Upload_Activity.this, "Archivo subido con exito", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String response, Throwable t) { // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    progreso.dismiss();
                    Toast.makeText(Upload_Activity.this, "Fallo o ya existia en el server", Toast.LENGTH_SHORT).show();

                }
            });
        }
    }

    //Lo tipico para saber si hay conexion
    private boolean hayInternet() {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            result = true;

        return result;

    }


}

