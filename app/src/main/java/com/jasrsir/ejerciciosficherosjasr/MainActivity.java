package com.jasrsir.ejerciciosficherosjasr;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickMain(View view) {
        switch (view.getId()) {
            case R.id.btnMainAgenda:
                intent = new Intent(MainActivity.this, Agenda_Activity.class);
                break;
            case R.id.btnMainAlarmas:
                intent = new Intent(MainActivity.this, Alarma_Activity.class);
                break;
            case R.id.btnMainMenstrual:
                intent = new Intent(MainActivity.this, Menstrual_Activity.class);
                break;
            case R.id.btnMainWeb:
                intent = new Intent(MainActivity.this, Web_Activity.class);
                break;
            case R.id.btnMainImage:
                intent = new Intent(MainActivity.this, Image_Activity.class);
                break;
            case R.id.btnMainMoney:
                intent = new Intent(MainActivity.this, Money_Activity.class);
                break;
            case R.id.btnMainUpload:
                intent = new Intent(MainActivity.this, Upload_Activity.class);
                break;
        }
        startActivity(intent);
    }
}
