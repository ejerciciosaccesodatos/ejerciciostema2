package com.jasrsir.ejerciciosficherosjasr.classes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by jasrsir on 17/11/16.
 */

public class SerialAmigos {
    //Campos
    private String ruta;
    private String archivo;

    public final static int SAVE_OK_CODE = 0;
    public final static int ERROR_IO_CODE = 1;

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String filename) {
        this.archivo = filename;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String path) {
        this.ruta = path;
    }

    //Constructor
    public SerialAmigos(String path, String filename) {
        this.ruta = path;
        this.archivo = filename;
    }

    //Métodos
    public int guardaAmigos() {

        int result = SAVE_OK_CODE;
        File file = new File(this.ruta, this.archivo);
        ObjectOutputStream outputStream = null;

        try {
            outputStream = new ObjectOutputStream(new FileOutputStream(file));

            for (int i = 0; i < Midiario.getMidiario().size(); i++)
                outputStream.writeObject(Midiario.getMidiario().get(i));

        } catch (IOException e) {
            result = ERROR_IO_CODE;
        } finally {

            if (outputStream != null) {

                try {
                    outputStream.close();
                } catch (IOException e) {}
            }
        }
        return result;
    }

    public void sacarDiario() {

        File file = new File(this.ruta, this.archivo);
        ObjectInputStream objectInputStream = null;
        Midiario.getMidiario().clear(); //Limpia la lista para evitar repeticiones

        try {
            if (!file.exists()) {
                file.createNewFile();
            } else {

                objectInputStream = new ObjectInputStream(new FileInputStream(file));
                //Saldrá del bucle cuando se produzca la excepcion
                while (true) {//Añade objetos a la lista
                    Midiario.getMidiario().add((Amigos) objectInputStream.readObject());
                }
            }
        } catch (Exception e) {

            try {
                //Cierra el flujo y de estar a null se controla en el catch
                objectInputStream.close();
            } catch (Exception e1) {

            }
        }
    }
}

