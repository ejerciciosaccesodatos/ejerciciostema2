package com.jasrsir.ejerciciosficherosjasr.classes;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jasrsir.ejerciciosficherosjasr.R;
import com.jasrsir.ejerciciosficherosjasr.classes.Amigos;
import com.jasrsir.ejerciciosficherosjasr.classes.Midiario;
import com.jasrsir.ejerciciosficherosjasr.classes.SerialAmigos;

public class AddAmigo extends AppCompatActivity {


        private static final String FILE_NAME  = "agenda.txt";
        private EditText nombre, phone, email;
        private TextInputLayout tilName, tilPhone, tilEmail;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_add_amigo);
            init();

        }

    private void init() {

        nombre = (EditText)findViewById(R.id.edtName);
        email = (EditText)findViewById(R.id.edtEmail);
        phone = (EditText)findViewById(R.id.edtphone);
        tilName = (TextInputLayout)findViewById(R.id.tilName);
        tilPhone = (TextInputLayout)findViewById(R.id.tilphone);
        tilEmail = (TextInputLayout)findViewById(R.id.tilEmail);


        nombre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilName.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilEmail.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                tilPhone.setError(null);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    public void onClickAddAmigo(View view) {

        String mail = email.getText().toString();

        //Valida el nombre
        if(nombre.getText().toString().isEmpty()){

            tilName.setError("No se permite un campo vacío");

            //Valida el telefono (6 digitos minimo)
        }else if(phone.getText().toString().length() < 6){

            tilPhone.setError("Debe tener 6 dígitos como mínimo, digo yo");

            //Valida el patron del email
        }else if(!Patterns.EMAIL_ADDRESS.matcher(mail).matches()){

            tilEmail.setError("Esto no es un email válido");

        }else{

            SerialAmigos amiguitos = new SerialAmigos(getApplicationContext().getFilesDir().getAbsolutePath(), FILE_NAME);
            Amigos amigos = new Amigos(nombre.getText().toString(), phone.getText().toString(), email.getText().toString());
            Midiario d = Midiario.getMidiario();
            d.add(amigos);
            amiguitos.guardaAmigos();
            Toast.makeText(this, "Tu amigo se ha guardado en " +amiguitos.getRuta() +"/"+ FILE_NAME, Toast.LENGTH_LONG).show();
            finish();
        }

    }
}
