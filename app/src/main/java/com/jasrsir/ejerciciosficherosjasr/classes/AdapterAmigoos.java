package com.jasrsir.ejerciciosficherosjasr.classes;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jasrsir.ejerciciosficherosjasr.R;

/**
 * Created by jasrsir on 18/11/16.
 */

public class AdapterAmigoos extends ArrayAdapter {


    Context context;
    Midiario midiario;

    public AdapterAmigoos(Context context, Midiario data) {
        super(context, R.layout.item_amigo, data);

        this.context = context;
        this.midiario = data;

    }


    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        //Instanciamos y asignamos los valores a los campos
        TextView txvName, txvTelephone, txvEmail;
        LayoutInflater inflater = LayoutInflater.from(context);
        View vi = inflater.inflate(R.layout.item_amigo, null);
        ImageButton imageButton = (ImageButton)vi.findViewById(R.id.btnDeleteAmigo);
        ImageButton btnEliminar = (ImageButton)vi.findViewById(R.id.btnDeleteAmigo);

        Amigos amigo = midiario.get(position);

        txvName = (TextView)vi.findViewById(R.id.tvxName);
        txvTelephone = (TextView)vi.findViewById(R.id.tvxPhone);
        txvEmail = (TextView)vi.findViewById(R.id.tvxEmail);
        txvName.setText(amigo.getName());
        txvTelephone.setText(amigo.getPhone());
        txvEmail.setText(amigo.getEmail());

        return vi;


    }

    public void onClickDeleteFriend(View view) {
        midiario.remove(view.getId());
        notifyDataSetChanged();
    }



}