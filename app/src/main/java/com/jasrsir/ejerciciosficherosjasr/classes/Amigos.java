package com.jasrsir.ejerciciosficherosjasr.classes;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by jasrsir on 18/11/16.
 */

public class Amigos implements Serializable {

    //Allvariables, getters and setters
    private String id;
    private String name;
    private String phone;
    private String email;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String telephono) {
        this.phone = telephono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }



    public Amigos(String name, String phone, String email) {

        this.name = name;
        this.phone = phone;
        this.email = email;
        this.id = UUID.randomUUID().toString();

    }

    @Override
    public boolean equals(Object o) {

        boolean result = false;
        Amigos f;

        if(o != null){

            if(o instanceof Amigos){

                f = (Amigos)o;

                if(this.id == f.id){

                    result = true;
                }
            }
        }
        return result;

    }



}
