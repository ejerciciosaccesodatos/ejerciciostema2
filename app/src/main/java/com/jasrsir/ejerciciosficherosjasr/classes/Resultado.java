package com.jasrsir.ejerciciosficherosjasr.classes;

/**
 * Clase resultado que ofrece información sobre el resultado al visitar una pagina web
 */
public class Resultado {

    private boolean codigo;
    private String contenido;
    private String mensaje;

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public boolean isCodigo() {
        return codigo;
    }

    public void setCodigo(boolean codigo) {
        this.codigo = codigo;
    }

}
