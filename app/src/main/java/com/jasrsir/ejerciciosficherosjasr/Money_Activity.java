package com.jasrsir.ejerciciosficherosjasr;

import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

public class Money_Activity extends AppCompatActivity {

    //Creacion de variables necesarias
    private EditText dolares;
    private EditText euros;
    private RadioButton dolarEuro;
    private static final String URL = "http://api.fixer.io/latest";
    private RadioButton euroDolar;
    private Button convertir;
    TextView resulado;
    private Toast notificacion;
    private double ratio;


    //Oncreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money);
        //inicializamos las variables con sus componentes correspondientes.
        dolares = (EditText) findViewById(R.id.txtDolar);
        euros = (EditText) findViewById(R.id.txtEuro);
        dolarEuro = (RadioButton) findViewById(R.id.rbtnDoltoEur);
        euroDolar = (RadioButton) findViewById(R.id.rbtnEuroDolar);
        convertir = (Button) findViewById(R.id.button);
        resulado = (TextView) findViewById(R.id.textView);

        if(!hayInternet())
            Toast.makeText(Money_Activity.this, "No hay una conexion a internet para comprobar los ratios", Toast.LENGTH_LONG).show();
        else
            conexion();
    }

    //Método OnClick para botones
    public void getOnClick(View v) {

        try {
            if (v == convertir){
                if (dolarEuro.isChecked()){
                    String valor = dolares.getText().toString();
                    if (valor.isEmpty() || valor == ".")
                        valor = "0";
                    else
                        euros.setText( String.valueOf( Double.parseDouble(valor)/ratio));
                }
                else {
                    String valor = euros.getText().toString();
                    if (valor.isEmpty() || valor == ".")
                        valor = "0";
                    else
                        dolares.setText( String.valueOf(ratio* Double.parseDouble(valor)));
                }
            }
        } catch (NumberFormatException exception){
            notificacion = Toast.makeText(this, "Error al convertir el número", Toast.LENGTH_LONG);
            notificacion.show();
        }
    }

    private boolean hayInternet() {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            result = true;

        return result;

    }

    //Tarea para descargar los datos
    private void conexion(){


        RequestQueue queue = Volley.newRequestQueue(Money_Activity.this);
        final ProgressDialog progress = new ProgressDialog(Money_Activity.this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                try {

                    JSONObject ratios = response.getJSONObject("rates"); //Extrae el objeto JSON con los ratios
                    String fecha = response.getString("date"); //Extraigo la fecha
                    String valor = ratios.getString("USD"); //Es te es el ratio que buscamos
                    resulado.setText("Fecha: "+fecha+" 1 Euro equivale a "+valor+" Dolares"); //Informo
                    ratio = Double.parseDouble(valor); //Convierto


                } catch (JSONException e) {

                    //Si se produjo un error en la lectura
                    Toast.makeText(Money_Activity.this, "Se produjo un error en la lectura del archivo", Toast.LENGTH_LONG).show();

                } catch (NumberFormatException e){

                    //Por si hay un error en la conversion que nunca se sabe
                    Toast.makeText(Money_Activity.this, "Los datos del archivo provocaron un error de conversión", Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Si hay un error
                Toast.makeText(Money_Activity.this, "Se produjo el siguiente error "+error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        queue.add(jsonObjectRequest); //A la cola

    }
}
