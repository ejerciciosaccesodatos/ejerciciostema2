package com.jasrsir.ejerciciosficherosjasr;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;


import android.widget.TextView;

import cz.msebera.android.httpclient.Header;

import com.jasrsir.ejerciciosficherosjasr.classes.Conexion;
import com.jasrsir.ejerciciosficherosjasr.classes.Streaming;
import com.loopj.android.http.*;


import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jasrsir.ejerciciosficherosjasr.classes.RestClient;
import com.jasrsir.ejerciciosficherosjasr.classes.Resultado;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

public class Web_Activity extends AppCompatActivity implements View.OnClickListener {

    private EditText direccion;
    private RadioButton radioJava;
    private RadioButton radioAAHC, radioVolley;

    private Button conectar, guardar;
    private RelativeLayout relativeLayout;
    private WebView web;
    private EditText nombrearchivo;
    StringBuilder contentWeb;

    private static final String JAVA = "JAVA";
    public static final String TAG = "My tag";
    RequestQueue mRequestQueue;


    TareaAsincrona miTareaAsinc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        init();
    }


    //Enlaza los objetos con el xml
    private void init() {
        direccion = (EditText) findViewById(R.id.direccion);
        radioJava = (RadioButton) findViewById(R.id.radioJava);
        radioAAHC = (RadioButton) findViewById(R.id.radioAAHC);
        radioVolley = (RadioButton) findViewById(R.id.radioVolley);
        conectar = (Button) findViewById(R.id.conectar);
        guardar = (Button) findViewById(R.id.btnGuardar);
        conectar.setOnClickListener(this);
        nombrearchivo = (EditText) findViewById(R.id.nombreArchivo);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeguardar);
        web = (WebView) findViewById(R.id.web);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Streaming guardar = new Streaming(Environment.getExternalStorageDirectory().getAbsolutePath());

                if (TextUtils.isEmpty(nombrearchivo.getText().toString()))
                    Toast.makeText(Web_Activity.this, "Tienes que darle un nombre al archivo", Toast.LENGTH_SHORT).show();
                else {

                    int result = guardar.escribeArchivo(nombrearchivo.getText().toString(), web.toString(), false);

                    switch (result) {

                        case Streaming.OK:
                            Toast.makeText(Web_Activity.this, "guardado satisfactorio en " + guardar.getRuta() + "/" + nombrearchivo.getText(), Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            }
        });
    }

    ;

    @Override
    public void onClick(View view) {
        String texto = direccion.getText().toString();
        direccion.clearFocus();
        String tipo = JAVA;
        web.clearChildFocus(web);
        if (radioAAHC.isChecked()) {
            AAHC();
        } else if (radioJava.isChecked()) {
            miTareaAsinc = new TareaAsincrona(this, "UTF-8");
            miTareaAsinc.execute(tipo, texto);
        } else if (radioVolley.isChecked())
            makeNetworkRequest(direccion.getText().toString());

        relativeLayout.setVisibility(View.VISIBLE);
    }


    //TODO CLASE ASÍNCRONA a JAVA

    //TODO CLASE ASÍNCRONA a HTTP
    public class TareaAsincrona extends AsyncTask<String, Integer, Resultado> {

        private ProgressDialog progreso;
        private Context context;
        private String encoding;


        public TareaAsincrona(Context context, String encoding) {
            this.context = context;
            this.encoding = encoding;
        }

        protected void onPreExecute() {
            progreso = new ProgressDialog(context);
            progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progreso.setMessage("Conectando . . .");
            progreso.setCancelable(false);

            progreso.show();
        }

        protected Resultado doInBackground(String... cadena) {
            Resultado resultado = null;
            int i = 1;
            try {
                // operaciones en el hilo secundario
                publishProgress(i++);
                resultado = Conexion.conectarJava(cadena[1]);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                resultado = new Resultado();
                resultado.setCodigo(false);
                resultado.setMensaje(e.getMessage());
            }
            return resultado;
        }

        protected void onProgressUpdate(Integer... progress) {
            progreso.setMessage("Conectando . . . " + Integer.toString(progress[0]));
        }

        protected void onPostExecute(Resultado resultado) {
            progreso.dismiss();
            // mostrar el resultado
            if (resultado.isCodigo()) {

                web.loadDataWithBaseURL(null, resultado.getContenido(), "text/html", "UTF-8", null);
                contentWeb = new StringBuilder(resultado.getContenido());
            }
            else
                web.loadDataWithBaseURL(null, resultado.getMensaje(), "text/html", "UTF-8", null);
        }

        protected void onCancelled() {
            progreso.dismiss();
            // mostrar cancelación
            web.loadDataWithBaseURL(null, "CANCELADO", "text/html", "UTF-8", null);
        }
    }

    //TODO CLASE AsynHTTPCLient
    private void AAHC() {
        final String texto = direccion.getText().toString();
        final ProgressDialog progreso = new ProgressDialog(Web_Activity.this);

        RestClient.get(texto, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                //progreso.setCancelable(false);
                progreso.setCancelable(false);

                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {
                // called when response HTTP status is "200 OK"
                progreso.dismiss();
                web.loadDataWithBaseURL(texto, response, "text/html", "UTF-8", null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();
                web.loadDataWithBaseURL(texto, "FALLO: " + response + " " + t.getMessage(), "text/html", "UTF-8", null);
            }
        });
    }

    public void makeSimpleRequest(String url) {
        final String enlace = url;
        // Instantiate the RequestQueue.
        mRequestQueue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        web.loadDataWithBaseURL(enlace, response, "text/html", "utf-8", null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message = "";
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            message = "Timeout Error " + error.getMessage();
                        } else if (error instanceof AuthFailureError) {
                            message = "AuthFailure Error " + error.getMessage();
                        } else if (error instanceof ServerError) {
                            message = "Server Error " + error.getMessage();
                        } else if (error instanceof NetworkError) {
                            message = "Network Error " + error.getMessage();
                        } else if (error instanceof ParseError) {
                            message = "Parse Error " + error.getMessage();
                        }
                        web.loadDataWithBaseURL(null, message, "text/html", "utf-8", null);
                    }
                });
        // Set the tag on the request.
        stringRequest.setTag(TAG);
        // Set retry policy
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        // Add the request to the RequestQueue.
        mRequestQueue.add(stringRequest);
    }

    public void makeNetworkRequest(String url) {
        final String enlace = url;
        RequestQueue mRequestQueue;
// Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
// Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
// Instantiate the RequestQueue with the cache and network.
        mRequestQueue = new RequestQueue(cache, network);
// Start the queue
        mRequestQueue.start();
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        web.loadDataWithBaseURL(enlace, response, "text/html", "utf-8", null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message = "";
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            message = "Timeout Error " + error.getMessage();
                        } else if (error instanceof AuthFailureError) {
                            message = "AuthFailure Error " + error.getMessage();
                        } else if (error instanceof ServerError) {
                            message = "Server Error " + error.getMessage();
                        } else if (error instanceof NetworkError) {
                            message = "Network Error " + error.getMessage();
                        } else if (error instanceof ParseError) {
                            message = "Parse Error " + error.getMessage();
                        }
                        web.loadDataWithBaseURL(null, message, "text/html", "utf-8", null);
                    }
                });
        // Set the tag on the request.
        stringRequest.setTag(TAG);
        // Set retry policy
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        // Add the request to the RequestQueue.
        mRequestQueue.add(stringRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }
}




































