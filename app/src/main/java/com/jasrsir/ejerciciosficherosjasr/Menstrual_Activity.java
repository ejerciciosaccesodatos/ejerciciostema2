package com.jasrsir.ejerciciosficherosjasr;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jasrsir.ejerciciosficherosjasr.classes.Streaming;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Menstrual_Activity extends AppCompatActivity {

    private Calendar fechaSelec;
    private Calendar fechaActual;
    private static int dia;
    private static int mes;
    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
    private static int anio;
    private TextView txvResult;
    private CalendarView fecha;
    private NumberPicker duracion;
    private Streaming grabador;
    private static final String FILE_NAME = "monstruorojo.txt";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menstrual);
        init();
    }

    private void init() {

        duracion = (NumberPicker) findViewById(R.id.numberPicker);
        fecha = (CalendarView) findViewById(R.id.calendMenstrual);
        duracion.setMinValue(25);
        duracion.setMaxValue(40);
        fechaActual = new GregorianCalendar(Locale.getDefault());
        txvResult = (TextView) findViewById(R.id.txvDiasSeleccionados);

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            grabador = new Streaming(Environment.getExternalStorageDirectory().getAbsolutePath());
        }

        fecha.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int year, int month, int day) {
                dia = day;
                mes = month;
                anio = year;
                fechaSelec = new GregorianCalendar(year, month, day);

                if (fechaSelec.getTimeInMillis() < fechaActual.getTimeInMillis())
                    actualizar(dia, mes, anio);
                else
                    Toast.makeText(Menstrual_Activity.this, "No puedes poner una fecha mayorque hoy", Toast.LENGTH_SHORT).show();

            }
        });

        duracion.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                actualizar(dia, mes, anio);
            }
        });
    }

    private void actualizar(int dia, int mes, int anio) {

        String fraseAux = "";
        String fechaAux = "";
        Calendar fechaNueva = new GregorianCalendar(anio, mes, dia);
        String problema = " HOY: ";
        Calendar[] dates = new Calendar[20];


        fechaNueva.add(Calendar.DAY_OF_MONTH, duracion.getValue()); //Ajustamos el perido de ovulacion
        //Cada posicion del array guarda una de las fechas
        dates[0] = (Calendar) fechaNueva.clone();
        dates[0].add(Calendar.DAY_OF_MONTH, -2);
        dates[1] = (Calendar) fechaNueva.clone();
        dates[1].add(Calendar.DAY_OF_MONTH, -1);
        dates[2] = fechaNueva; //La segunda guarda la fecha de la que partimos para el calculo
        dates[3] = (Calendar) fechaNueva.clone();
        dates[3].add(Calendar.DAY_OF_MONTH, 1);

        grabador.escribeArchivo(FILE_NAME, String.format(Locale.getDefault(), "dd/MM/yyyy", dates[0].getTime()) + "\n", false);
        grabador.escribeArchivo(FILE_NAME, String.format(Locale.getDefault(), "dd/MM/yyyy", dates[3].getTime()), true);
        Toast.makeText(this, "Tu fechas se han guardado en  " +grabador.getRuta() +"/"+ FILE_NAME, Toast.LENGTH_LONG).show();


        txvResult.setText("Estas son las fechas de tu menstruación para los próximos 5 meses" + "\n" + "\n");
        txvResult.append("\n" + "Mes " + fechaNueva.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + "\n" + "\n");
        for (int i = 0; i < 4; i++) {
            fraseAux = formatoFecha.format(dates[i].getTime());
            fechaAux = formatoFecha.format(fechaActual.getTime());
            if (fraseAux.equals(fechaAux))
                txvResult.append(problema + dates[i].getDisplayName(dates[i].DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()) + " " + fraseAux.substring(0, 2) + " de " +
                        dates[i].getDisplayName(dates[i].MONTH, Calendar.LONG, Locale.getDefault()) + " de " + fraseAux.substring(6, 10) + "\n");
            else
                txvResult.append(dates[i].getDisplayName(dates[i].DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()) + " " + fraseAux.substring(0, 2) + " de " +
                        dates[i].getDisplayName(dates[i].MONTH, Calendar.LONG, Locale.getDefault()) + " de " + fraseAux.substring(6, 10) + "\n");
        }

        for (int i = 1; i < 5; i++) {
            fechaNueva.add(Calendar.DAY_OF_MONTH, duracion.getValue());
            fraseAux = formatoFecha.format(fechaNueva.getTime());
            txvResult.append("\n" + " Mes " + fechaNueva.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + "\n" + "\n");
            dates[0 + (i * 3) + i] = (Calendar) fechaNueva.clone();
            dates[0 + (i * 3) + i].add(Calendar.DAY_OF_MONTH, -2);
            dates[1 + (i * 3) + i] = (Calendar) fechaNueva.clone();
            dates[1 + (i * 3) + i].add(Calendar.DAY_OF_MONTH, -1);
            dates[2 + (i * 3) + i] = fechaNueva;
            dates[2 + (i * 3) + i].add(Calendar.DAY_OF_MONTH, 0);
            dates[3 + (i * 3) + i] = (Calendar) fechaNueva.clone();
            dates[3 + (i * 3) + i].add(Calendar.DAY_OF_MONTH, 1);

            for (int j = (0 + (i * 3) + i); j <= (3 + (i * 3) + i); j++) {
                fraseAux = formatoFecha.format(dates[i].getTime());
                fechaAux = formatoFecha.format(fechaActual.getTime());
                if (fraseAux.equals(fechaAux))
                    txvResult.append(problema + dates[i].getDisplayName(dates[i].DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()) + " " + fraseAux.substring(0, 2) + " de " +
                            dates[i].getDisplayName(dates[i].MONTH, Calendar.LONG, Locale.getDefault()) + " de " + fraseAux.substring(6, 10) + "\n");
                else
                    txvResult.append(dates[j].getDisplayName(dates[j].DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()) + " " + formatoFecha.format(dates[j].getTime()).substring(0, 2) + " de " +
                            dates[j].getDisplayName(dates[j].MONTH, Calendar.LONG, Locale.getDefault()) + " de " + formatoFecha.format(dates[j].getTime()).substring(6, 10) + "\n");
            }
        }


    }
}



