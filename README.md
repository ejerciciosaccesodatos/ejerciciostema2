# LEEME:  #

Ejercicios del tema 2 de Juan Antonio Suarez Rosa

### Ejercicios que contiene ###

1. Agenda de amigos
2. Alarmas
3. Menstrualidad
4. Pagina Web
5. Imagenes
6. Moneda
7. Subir Archivos
 


### ¿Qué contienen / planteamiento lleva? ###

* Cada ejercicio te marcará donde se guarda el archivo.

**Agenda Amigos**

* Contiene una activity para añadir un amigo y se añadirán al listView y a un fichero que se marcará en un Toast. (No se eliminan)

**Alarmas**

* Las alarmas están configuradas para que suenen 5 en el mismo intervalo de tiempo que hayas puesto la primera vez. 

**Menstrualidad**

* Este ejercicio te guarda en el fichero los dias del proximo mes, aunque en este ejercicio muestro los dias de la menstrualidad de los siguientes 5 meses.

**Web**

* En este he tenido un problema y es que no se porqué pero una vez que accedes a una pagina ya no puedes editar la url de nuevo.. teniendo que salir de esa activity y volver a entrar....

**Imagenes**

* Las imagenes son las que vienen en el planteamiento de los ejercicios, la ruta se la he pedido a amador donde están los 4 enlaces. creo que 2 ya no están operativas.
* Se puede guardar una imagen en la ruta que le aparecera.

**Monedas**

* En este ejercicio se coge el valor de internet de una pagina que me ha comentado daniel acedo. Funciona bien

**Subir Archivos**

* En este ejercicio se suben archivos a la dirección 
https://ejerciciostema2.000webhostapp.com/uploads/
 Los errores estan controlados


*El ejercicio de copiar un archivo con y sin buffer no lo he llegado a realizar por falta de tiempo. lo siento*